﻿namespace CruzScripts.Scripts.Models
{
    public interface IModel
    {
        ModelType Type { get; }
        void Initialize();
        void OnStart();
        void OnUpdate();
        void OnAwake();
        void Save();
        void Load();
    }
}