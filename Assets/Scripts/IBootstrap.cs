﻿using System.Collections.Generic;
using CruzScripts.Scripts.Controller;
using CruzScripts.Scripts.Models;
using CruzScripts.Scripts.Service;

namespace CruzScripts.Scripts
{
    public enum ControllerType {
        Input,
        Factory,
        WorldCanvas,
        Score
    }
    public enum ModelType { }
    public enum ServiceType { }

    public interface IBootstrap
    {
        event Bootstrap.SimpleEvent Initialized;
        List<IController> Controllers { get; }
        List<IModel> Models { get; }
        List<IService> Services { get; }

        void Initialize();

        void SetController(IController value);
        IController GetController(ControllerType type);
        void SetModel(IModel value);
        IModel Getmodel(ModelType type);
        void SetService(IService value);
        IService GetSerice(ServiceType type);
    }
}