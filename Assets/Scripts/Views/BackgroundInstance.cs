﻿using UnityEngine;

public class BackgroundInstance : MonoBehaviour
{
    Transform _transform;
    public SpriteRenderer renderer;

    public GameObject background;
    public bool used = false;

    void Start()
    {
        _transform = GetComponent<Transform>();
        renderer = GetComponent<SpriteRenderer>();
    }

    public Vector3 GetPosition()
    {
        return _transform.position;
    }

    public void SetPosition(Vector3 newPosition)
    {
        _transform.localPosition = newPosition;
    }
}
