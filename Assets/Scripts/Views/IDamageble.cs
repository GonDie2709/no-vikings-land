﻿using UnityEngine;

namespace CruzScripts.Scripts.Views
{
    public interface IDamageble
    {
        bool GetIsEnemy();
        GameObject GetGameObject();
        int GetHP();
        int GetMaxHP();
        bool invincible { get; set; }
        event Bootstrap.SimpleEvent Hited, Died;
        void Heal(int value);
        void OnHit(int value);
        void OnDie();
    }
}