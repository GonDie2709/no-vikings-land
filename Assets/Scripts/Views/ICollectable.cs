﻿namespace CruzScripts.Scripts.Views
{
    public interface ICollectable
    {
        void OnCollect(IView Other);
    }
}