﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreditScreen : MonoBehaviour
{
    public FadeStart fade;
    public Text score;

	// Use this for initialization
	void Start ()
    {
        fade.FadeInOut(true, 0f);
        fade.FadeInOut(false, 5f);

        score.text = "Score: " + ScoreHolder.score.ToString("000000");
    }
}
