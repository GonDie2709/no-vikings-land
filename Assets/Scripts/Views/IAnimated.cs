﻿using UnityEngine;

namespace CruzScripts.Scripts.Views
{
    public interface IAnimated
    {
        Animator animator { get; }
        void SetSpeed(float speed);
        void SetDoAction();
    }
}