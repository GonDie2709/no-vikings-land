﻿using UnityEngine;
using System.Collections;
using CruzScripts.Scripts.Views;
using CruzScripts.Scripts.Views.Creature;

public class HandsController : MonoBehaviour
{
    AudioSource _audio;
    CreatureBase entity;
    public bool IsEnemy;
    public int damage = 20;

	// Use this for initialization
	void Start ()
    {
        if (transform.parent.GetComponent<CreatureBase>() != null)
            entity = transform.parent.GetComponent<CreatureBase>();
        else
            entity = transform.parent.parent.GetComponent<CreatureBase>();

        _audio = GetComponent<AudioSource>();
    }
	
    void AttackStart()
    {
        ((Player)entity).weaponCollider.enabled = true;

        if (_audio)
            _audio.Play();
    }

    void AttackEnd()
    {
        entity.PrimaryAttackReset();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<IDamageble>() != null)
        {
            if (collision.gameObject.GetComponent<IDamageble>().GetIsEnemy() == IsEnemy)
            {
                //collision.gameObject.GetComponent<IDamageble>().OnHit(damage);
            }
            else
            {
                collision.gameObject.GetComponent<IDamageble>().OnHit(damage);

                if (_audio && IsEnemy)
                    _audio.Play();
            }
        }
    }
}
