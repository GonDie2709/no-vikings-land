﻿using CruzScripts.Scripts.Controller;
using DG.Tweening;
using UnityEngine;

namespace CruzScripts.Scripts.Views.Creature
{
    public class DashEnemy : BaseEnemy
    {
        [Space]
        public float DashSpeed;

        private int mininsDead;
        private bool canDash;

        public override void OnAwake()
        {
            base.OnAwake();

            canDash = true;

            System.Action dash = () => Dash();
            _blackboard.Set("dash", dash, _tree.id);
            _blackboard.Set("canDash", true, _tree.id);
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            _blackboard.Set("canDash", canDash, _tree.id);
        }

        public void Dash()
        {
            canDash = false;

            var target = (CreatureBase)_blackboard.Get("target", _tree.id).value;
            var dir = -transform.position + target.transform.position;
            var ray = new Ray(transform.position, dir);
            var dist = Vector3.Distance(transform.position, target.transform.position);

            transform.DOMove(ray.GetPoint(dist*1.6f), DashSpeed).SetSpeedBased().Play().OnComplete(DashEnd);
        }

        private void DashEnd()
        {
            canDash = true;
        }
    }
}