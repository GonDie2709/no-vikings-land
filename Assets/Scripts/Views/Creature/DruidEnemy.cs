﻿using CruzScripts.Scripts.Controller;
using UnityEngine;

namespace CruzScripts.Scripts.Views.Creature
{
    public class DruidEnemy : BaseEnemy
    {
        [Space]
        public EnemyType SpawnType;
        public Transform[] SpawnPoints;

        private int mininsDead;

        public override void OnAwake()
        {
            base.OnAwake();

            System.Action spawnE = () => SapwnEnemies();
            _blackboard.Set("spawn", spawnE, _tree.id);
            _blackboard.Set("canSpawn", true, _tree.id);
        }

        public override void OnStart()
        {
            base.OnStart();
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            _blackboard.Set("canSpawn", mininsDead >= SpawnPoints.Length, _tree.id);
        }

        public override void Spawn(Vector3 position)
        {
            base.Spawn(position);

            mininsDead = SpawnPoints.Length;
        }

        void SapwnEnemies()
        {
            mininsDead = 0;

            for (int i = 0; i < SpawnPoints.Length; i++)
            {
                var minion = factory.InstantiateEnemy(SpawnType, false);
                minion.Spawn(SpawnPoints[i].position);
                minion.Died += MinionDied;
            }
        }

        private void MinionDied()
        {
            mininsDead ++;
        }
    }
}