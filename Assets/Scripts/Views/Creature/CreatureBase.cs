﻿using UnityEngine;

namespace CruzScripts.Scripts.Views.Creature
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class CreatureBase : GenericView, ICreature, IDamageble
    {
        public Rigidbody2D rigidbody { get; protected set; }

        [HideInInspector]
        public bool isAttacking;

        public bool isDead { get; protected set; }
        public float speed;
        public int maxHp;
        public int hp;
        public bool isEnemy;
        public GameObject deathXplosion;

        [HideInInspector] public BaseItem DroppableItem;

        public delegate void CreatureBaseEvent(CreatureBase obj);

        public event Bootstrap.SimpleEvent Hited;
        public event Bootstrap.SimpleEvent Died;
        public event CreatureBaseEvent DiedCreature;


        [HideInInspector]
        public bool invincible { get; set; }

        public override void OnAwake()
        {
            base.OnAwake();

            rigidbody = GetComponent<Rigidbody2D>();
            invincible = false;
        }
        public override void OnStart()
        {
            //Spawn(transform.position);
            base.OnStart();
        }
        private void SetDummyData()
        {
            hp = 10;
            speed = 50;
        }

        public virtual void Spawn(Vector3 position)
        {
            isDead = false;
            transform.position = position;
            hp = maxHp;
        }

        public virtual void DoMove(Vector2 value)
        {
            var mov = new Vector3(value.x, value.y, 0);
            mov = Vector3.ClampMagnitude(mov, 1);
            mov = mov * Time.deltaTime * speed;
            rigidbody.angularVelocity = 0f;
            rigidbody.velocity = mov;

            //SetSpeed(mov.magnitude);
        }

        public virtual void DoStay(){ }

        public float GetSpeed()
        {
            throw new System.NotImplementedException();
        }

        public bool GetIsEnemy()
        {
            return isEnemy;
        }

        public GameObject GetGameObject()
        {
            return this.gameObject;
        }

        public int GetHP()
        {
            return hp;
        }
        public int GetMaxHP()
        {
            return maxHp;
        }

        public virtual void DoAtack()
        {
            Debug.Log("Attack !!");
        }
        public virtual void PrimaryAttackReset()
        {
            isAttacking = false;
        }

        public virtual void DoDefend()
        {
            throw new System.NotImplementedException();
        }
        public virtual void DoDefendRelease()
        {
            throw new System.NotImplementedException();
        }
        public virtual void DoJump()
        {
            throw new System.NotImplementedException();
        }
        
        public virtual void DoAction()
        {
            throw new System.NotImplementedException();
        }
        public virtual void DoLock()
        {
            throw new System.NotImplementedException();
        }

        public void DoShout()
        {
            //Debug.Log("Shout !!!");
        }

        public virtual void DoLock(bool value)
        {
            throw new System.NotImplementedException();
        }

        public virtual void OnHit(int value)
        {
            hp -= value;

            if (Hited != null) Hited();
            if (hp <= 0)
            {
                OnDie();
            }
        }
        public virtual void OnDie()
        {
            isDead = true;

            if (Died != null) Died();
            if (DiedCreature != null) DiedCreature(this);
        }

        public virtual void Heal(int amount)
        {
            hp += amount;
            if (hp > maxHp) hp = maxHp;
        }
    }
}