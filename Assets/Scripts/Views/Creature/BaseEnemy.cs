﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using Assets.Scripts.Views;
using CruzScripts.Scripts.Controller;
using Enums;
using InterativaSystem.Treesharp;
using InterativaSystem.Treesharp.Actions;
using InterativaSystem.Treesharp.Composites;
using InterativaSystem.Treesharp.Conditions;
using InterativaSystem.Treesharp.Decorators;
using Newtonsoft.Json;
using UnityEngine;
using Behaviour = InterativaSystem.Treesharp.Behaviour;
using Random = UnityEngine.Random;

namespace CruzScripts.Scripts.Views.Creature
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(AudioSource))]
    public class BaseEnemy : CreatureBase, IAnimated, IDropable
    {
        protected ScoreController scoreController;
        protected Behaviour _tree;
        protected Blackboard _blackboard;
        protected AudioSource _audioSource;


        public Animator animator { get; protected set; }

        [Header("AI Settings")]
        public float AttackRange;
        public float BuddyRange;
        public float MinRadius;
        [Space]
        public int LowHpBehaviour;
        [Space]
        public WeaponType weaponType;
        public float WeaponCooldown;
        public float ProjectileDuration;
        public int ProjectileDamage;
        public int MagazineSize=10;
        public Projectile projectile;
        public Transform barrelPosition;
        public AudioClip ShootSFX;
        public AudioClip MeleeSFX, ExplosionSFX, SpawnSFX;

        [Space]
        public int Points = 10;

        [Space] public Animator SpawnFX, DeathFX;

        [Header("Animation")]
        public Animator handsAnimator;

        [Header("HUD")]
        public string HelthBarPrefab = "EnemyHealthBar";
        public Vector3 HelthBarOffset;

        Projectile[] bullets;

        protected EnemiesFactory factory;
        protected WorldCanvasController worldCanvas;
        
        public override void OnAwake()
        {
            base.OnAwake();

            isDead = true;

            _blackboard = new Blackboard();
            _tree = new Behaviour();

            _blackboard.Set("transform", transform, _tree.id);
            _blackboard.Set("isEnemy", isEnemy, _tree.id);

            System.Action shout = () => DoShout();
            System.Action attack = () => DoAtack();
            System.Action stay = () => DoStay();
            System.Action doPri = () => PlayMelee();
            System.Action move = () => DoMove((Vector2)_blackboard.Get("moveDir", _tree.id).value);

            _blackboard.Set("doShout", shout, _tree.id);
            _blackboard.Set("doStay", stay, _tree.id);
            _blackboard.Set("doAttack", attack, _tree.id);
            _blackboard.Set("doPrimaryAction", doPri, _tree.id);
            _blackboard.Set("doMove", move, _tree.id);

            _blackboard.Set("thisCreatureBase", this, _tree.id);
            _blackboard.Set("hp", hp, _tree.id);
            _blackboard.Set("speed", speed, _tree.id);
            _blackboard.Set("deltaTime", Time.deltaTime, _tree.id);
            _blackboard.Set("isDead", isDead, _tree.id);

            _tree.root = new Priority(
                new List<Node>
                {
                    new Inverter(new List<Node>()
                    {
                        new CheckIsDead(),
                    }),
                    //On terget aquire check distances and movements
                    new Sequence(new List<Node>()
                    {
                        new CheckDataNotNull("target"),
                        new CallSystemAction("doStay"),

                        new Priority(new List<Node>()
                        {
                            new Sequence(new List<Node>()
                            {
                                new CheckDistance(AttackRange, "target"),
                                new LookToTarget("target"),
                                new Inverter(new List<Node>()
                                {
                                    new CheckBool("isCoolingDown"),
                                }),
                                new Attack(),
                            }),

                            //Missing Check for nearby allies
                            
                            new Sequence(new List<Node>()
                            {
                                new CheckHP(LowHpBehaviour),
                                new Priority(new List<Node>()
                                {
                                    new Sequence(new List<Node>()
                                    {
                                        new CheckDataNotNull("nerarbyTarget"), 
                                        new DefineNearbyTarget(!isEnemy, "nerarbyTarget"),
                                        new Inverter(new List<Node>()
                                        {
                                            new CheckDistance(BuddyRange, "nerarbyTarget"),
                                        }),
                                        new LookToTarget("target"),
                                        new Move("nerarbyTarget"),
                                        //new MoveToTransform("nerarbyTarget")
                                    }),
                                    new Sequence(new List<Node>()
                                    {
                                        new Inverter(new List<Node>()
                                        {
                                            new CheckDataNotNull("nerarbyTarget"),
                                        }),
                                        new DefineNearbyTarget(!isEnemy, "nerarbyTarget")
                                    }),
                                    new MemorySequence(new List<Node>()
                                    {
                                        new LookToTarget("target"),
                                        new Wait(0.4f),
                                        new LookToTarget("target"),
                                        new ClearData("nerarbyTarget")
                                    })
                                }),
                            }),
                            
                            //Druids Behaviour
                            new MemorySequence(new List<Node>()
                            {
                                new CheckDataNotNull("spawn"),
                                new CheckBool("canSpawn"),
                                new Chance(0.02f),
                                new CallSystemAction("doPrimaryAction"),
                                new CallSystemAction("spawn"), 
                                new Wait(1.2f)
                            }),

                            //Dash Behaviour
                            new MemorySequence(new List<Node>()
                            {
                                new CheckDataNotNull("dash"), 
                                new CheckBool("canDash"),
                                new Chance(0.008f),
                                //new Wait(0.2f),
                                new CallSystemAction("doPrimaryAction"),
                                new CallSystemAction("dash"), 
                                new Wait(0.8f)
                            }),

                            new Sequence(new List<Node>()
                            {
                                new LookToTarget("target"),
                                new Inverter(new List<Node>()
                                {
                                    new CheckDistance(AttackRange, "target"),
                                }),
                                new Move("target")
                            })
                        }),
                    }),

                    //try to find a target
                    new Sequence(new List<Node>()
                    {
                        new Inverter(new List<Node>()
                        {
                            new CheckDataNotNull("target"),
                        }),
                        new DefineTarget(isEnemy),
                        new LookToTarget("target"),
                        new Shout(),
                    }),

                    //Stand and do nothing
                    new Stand()
                });
        }

        public override void OnStart()
        {
            SpawnFX.gameObject.SetActive(false);
            DeathFX.gameObject.SetActive(false);

            _audioSource = GetComponent<AudioSource>();

            base.OnStart();

            factory = bootstrap.GetController(ControllerType.Factory) as EnemiesFactory;
            scoreController = bootstrap.GetController(ControllerType.Score) as ScoreController;
            worldCanvas = bootstrap.GetController(ControllerType.WorldCanvas) as WorldCanvasController;

            worldCanvas.InstantiateHelthBar(HelthBarPrefab, this, HelthBarOffset);


            animator = GetComponent<Animator>();

            FillProjectiles();
        }
        public override void DoMove(Vector2 value)
        {
            var endpos = transform.position + new Vector3(value.x, value.y, 0) * speed * Time.deltaTime;

            var nearby = GameObject.FindObjectsOfType<CreatureBase>().ToList().FindAll(x => Vector3.Distance(x.transform.position, endpos) <= MinRadius && x != this);
            if (nearby.Count > 0)
            {
                var min = nearby.Min(x => Vector3.Distance(x.transform.position, endpos));
                var closer = nearby.Find(x => Vector3.Distance(x.transform.position, endpos) <= min);

                var dir = (endpos - closer.transform.position).normalized;

                value = new Vector2(value.x + dir.x, value.x + dir.x);
            }

            var speedAnim = new Vector3(value.x, value.y, 0).magnitude;
            SetSpeed(speedAnim);

            transform.position += new Vector3(value.x, value.y, 0) * speed * Time.deltaTime;
        }
        public override void DoStay()
        {
            base.DoStay();
            SetSpeed(0);
        }
        public override void OnUpdate()
        {
            _blackboard.Set("hp", hp, _tree.id);
            _blackboard.Set("deltaTime", Time.deltaTime, _tree.id);
            _blackboard.Set("isDead", isDead, _tree.id);
            _tree.Tick(this.gameObject, _blackboard);
        }
        public override void DoAtack()
        {
            _blackboard.Set("isCoolingDown", true, _tree.id);

            int count = 0;
            switch (weaponType)
            {
                case WeaponType.Melee:
                    PlayMelee();
                    break;
                case WeaponType.Pistol:
                    count = 0;
                    while (true)
                    {
                        if (!bullets[count].gameObject.activeSelf)
                        {
                            float rng = Random.Range(-5, 5);
                            Quaternion rotation = Quaternion.Euler(barrelPosition.eulerAngles + (Vector3.forward * rng));

                            bullets[count].Fire(barrelPosition.position, rotation, weaponType, ProjectileDuration, ProjectileDamage);

                            break;
                        }

                        count++;
                    }
                    PlayShoot();
                    break;
                case WeaponType.ShotgunAxe:
                    foreach (Projectile bullet in bullets)
                    {
                        float rng = Random.Range(-20, 20);
                        Quaternion rotation = Quaternion.Euler(barrelPosition.eulerAngles + (Vector3.forward * rng));

                        bullet.Fire(barrelPosition.position, rotation, weaponType, ProjectileDuration, ProjectileDamage);
                    }
                    PlayShoot();
                    break;
                case WeaponType.Macenigun:
                    count = 0;
                    while (true)
                    {
                        if (!bullets[count].gameObject.activeSelf)
                        {
                            float rng = Random.Range(-5, 5);
                            Quaternion rotation = Quaternion.Euler(barrelPosition.eulerAngles + (Vector3.forward * rng));

                            bullets[count].Fire(barrelPosition.position, rotation, weaponType, ProjectileDuration, ProjectileDamage);

                            break;
                        }
                        count++;
                    }
                    PlayShoot();
                    break;
            }

            StartCoroutine(ShotDelay());
        }

        public override void OnHit(int value)
        {
            if(isDead) return;

            base.OnHit(value);
        }

        void FillProjectiles()
        {
            bullets = new Projectile[MagazineSize];
            for (int i = 0; i < bullets.Length; i++)
            {
                bullets[i] = Instantiate(projectile).GetComponent<Projectile>();
                bullets[i].transform.parent = transform.parent;
            }
        }
        void PlayMelee()
        {
            if (handsAnimator)
                handsAnimator.SetTrigger("PrimaryAttack");
            animator.SetTrigger("PrimaryAttack");
        }
        void PlayShoot()
        {
            if (handsAnimator)
                handsAnimator.SetBool("SecondaryAttack", true);

            PlayShootSFX();
            animator.SetTrigger("SecondaryAttack");
        }

        IEnumerator ShotDelay()
        {
            float cooldown = 0f;

            switch (weaponType)
            {
                case WeaponType.Melee:
                    cooldown = WeaponCooldown;
                    break;
                case WeaponType.Pistol:
                    cooldown = WeaponCooldown;
                    break;
                case WeaponType.ShotgunAxe:
                    cooldown = WeaponCooldown;
                    break;
                case WeaponType.Macenigun:
                    cooldown = WeaponCooldown;
                    break;
            }

            yield return new WaitForSeconds(cooldown);

            _blackboard.Set("isCoolingDown", false, _tree.id);
        }

        public override void Spawn(Vector3 position)
        {
            base.Spawn(position);
            
            SpawnFX.gameObject.SetActive(true);
            SpawnFX.Play("FX");
            PlaySpawnSFX();

            Invoke("DisableSpawnFX", 0.7f);
        }

        public void DisableSpawnFX()
        {
            SpawnFX.gameObject.SetActive(false);
        }
        public void DisableDeathFX()
        {
            if(isDead)
                transform.position = factory.voidPosition;

            DeathFX.gameObject.SetActive(false);
        }

        public override void OnDie()
        {
            Drop();

            DeathFX.gameObject.SetActive(true);
            DeathFX.Play("FX");
            PlayExplosionSFX();

            Invoke("DisableDeathFX", 0.5f);

            scoreController.AddScore(Points);
            base.OnDie();
        }
        public void SetSpeed(float speed)
        {
            animator.SetFloat("Speed", speed);
        }
        public void SetDoAction()
        {
            throw new System.NotImplementedException();
        }

        public void PlayMeleeSFX()
        {
            if (isDead) return;

            _audioSource.clip = MeleeSFX;
            _audioSource.Play();
        }
        public void PlayShootSFX()
        {
            if (isDead) return;

            _audioSource.clip = ShootSFX;
            _audioSource.Play();
        }
        public void PlayExplosionSFX()
        {
            if (isDead) return;

            _audioSource.clip = ExplosionSFX;
            _audioSource.Play();
        }
        public void PlaySpawnSFX()
        {
            if (isDead) return;

            _audioSource.clip = SpawnSFX;
            _audioSource.Play();
        }

        public void Drop()
        {
            if (DroppableItem == null) return;

            Debug.Log("Drop   " + this);

            var temp = Instantiate(DroppableItem) as BaseItem;

            if (temp.GetComponent<WeaponSwap>())
            {
                var rnd = new System.Random();
                var index = rnd.Next(0, temp.GetComponent<WeaponSwap>().weaponImgs.Length);

                temp.GetComponent<WeaponSwap>().changeTo = (WeaponType)index;
            }

            temp.Initialize();
            temp.transform.position = transform.position;
            //temp.transform.localScale = Vector3.one * 0.5f;
            temp.transform.parent = transform.parent;
        }
    }
}