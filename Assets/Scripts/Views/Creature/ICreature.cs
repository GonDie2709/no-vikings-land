﻿using UnityEngine;

namespace CruzScripts.Scripts.Views.Creature
{
    public interface ICreature
    {
        Rigidbody2D rigidbody { get; }

        bool isDead { get; }

        float GetSpeed();

        void Spawn(Vector3 position);

        void DoMove(Vector2 value);
        void DoStay();
        void DoAtack();
        void DoDefend();
        void DoDefendRelease();
        void DoJump();
        void DoAction();
        void DoLock();
        void DoShout();
        void DoLock(bool value);
    }
}