﻿using CruzScripts.Scripts.Controller;
using Enums;
using System.Collections;
using UnityEngine;

namespace CruzScripts.Scripts.Views.Creature
{
    public class Player : CreatureBase
    {
        private InputController inputs;

        AudioSource _bulletAudio;
        AudioSource _speechAudio;
        public Collider2D weaponCollider;
        Animator handsAnimator;
        Animator footAnimator;
        Animator weaponAnimator;
        SpriteRenderer _renderer;
        SpriteRenderer weaponSprite;

        public GameObject projectile;

        public Transform shotgunBarrell;
        public Transform maceBarrell;
        public Transform swordBarrell;
        public Transform hammerBarrell;

        public ParticleSystem swordParticleEnd;

        public AudioClip[] bulletSfx;
        public AudioClip[] speechSfx;

        public GameObject portal;

        public float ammo;
        float totalAmmo;

        float laserLength = 12f;
        float laserBuildTime = 0.2f;
        float laserDuration = 0f;

        bool playingBeam = false;
        bool started = false;

        Projectile[] bullets = new Projectile[65];

        LineRenderer line;

        WeaponType _weaponType;
        public WeaponType weaponType
        {
            get { return _weaponType; }
            set
            {
                ReleaseSecondaryAttack();
                ChangeWeapon(value);
                _weaponType = value;
            }
        }

        public Sprite[] weaponSprites;
        public bool isCoolingDown = false;
        Coroutine coolingRoutine;

        public override void OnStart()
        {
            base.OnStart();

            bootstrap.player = this;
            inputs = bootstrap.GetController(ControllerType.Input) as InputController;

            _renderer = GetComponent<SpriteRenderer>();

            _bulletAudio = GetComponents<AudioSource>()[0];
            _speechAudio = GetComponents<AudioSource>()[1];

            footAnimator = transform.FindChild("Feets").GetComponent<Animator>();
            handsAnimator = transform.FindChild("HandsContainer").GetComponent<Animator>();
            weaponAnimator = transform.FindChild("HandsContainer/Hands/Weapon").GetComponent<Animator>();
            weaponSprite = transform.FindChild("HandsContainer/Hands/Weapon").GetComponent<SpriteRenderer>();

            weaponCollider = transform.FindChild("HandsContainer").GetComponent<Collider2D>();

            inputs.OnJumpPress += DoJump;
            inputs.OnFirePress += DoSecondaryAttack;
            inputs.OnFire2Press += DoAtack;
            inputs.OnFireRelease += ReleaseSecondaryAttack;

            Died += Dead;

            for (int i = 0; i < bullets.Length; i++)
            {
                bullets[i] = Instantiate(projectile).GetComponent<Projectile>();
            }

            //line = transform.FindChild("HandsContainer/Hands/Weapon/SwordBarrell/Laser").GetComponent<LineRenderer>();
            line = GetComponent<LineRenderer>();
            line.sortingLayerName = "Projectiles";
            line.sortingOrder = 2;

            ShowHide();
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            DoMove(inputs.MainAxis);
            Vector3 mov = Vector3.ClampMagnitude(inputs.MainAxis, 1);
            footAnimator.SetBool("isWalking", System.Convert.ToBoolean(mov.magnitude));

            MouseLook();
        }

        void MouseLook()
        {
            Vector3 mouseAt = Input.mousePosition;
            Vector2 object_pos = Camera.main.WorldToScreenPoint(transform.position);

            mouseAt.x = mouseAt.x - object_pos.x;
            mouseAt.y = mouseAt.y - object_pos.y;

            float angle = Mathf.Atan2(mouseAt.y, mouseAt.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, angle), 10f * Time.deltaTime);
        }

        void ChangeWeapon(WeaponType toType)
        {
            if (coolingRoutine != null) StopCoroutine(coolingRoutine);
            isCoolingDown = false;
            weaponSprite.sprite = weaponSprites[(int)toType];

            SetWeaponCollider(toType);

            totalAmmo = GetAmmo(toType);
            ammo = totalAmmo;

            if(bootstrap.HUD != null)
                bootstrap.HUD.SetAmmo(ammo / totalAmmo);

            _speechAudio.clip = speechSfx[(int)toType];
            _speechAudio.Play();

            weaponAnimator.SetBool("ShotgunActive", false);
            weaponAnimator.SetBool("MaceActive", false);
            weaponAnimator.SetBool("SwordActive", false);
            weaponAnimator.SetBool("HammerActive", false);

            switch (toType)
            {
                case WeaponType.ShotgunAxe:
                    weaponAnimator.SetBool("ShotgunActive", true);
                    break;
                case WeaponType.Macenigun:
                    weaponAnimator.SetBool("MaceActive", true);
                    break;
                case WeaponType.SwordLaser:
                    weaponAnimator.SetBool("SwordActive", true);
                    break;
                case WeaponType.Hammissile:
                    weaponAnimator.SetBool("HammerActive", true);
                    break;
            }

            started = true;
        }

        public override void DoAtack()
        {
            if (isAttacking) return;
            isAttacking = true;

            PrimaryAttack();
        }

        void DoSecondaryAttack()
        {
            if (isAttacking || isCoolingDown || ammo <= 0) return;
            isCoolingDown = true;

            SecondaryAttack();
            coolingRoutine = StartCoroutine(ShotDelay());
        }

        void PrimaryAttack()
        {
            handsAnimator.SetTrigger("PrimaryAttack");
        }

        void SecondaryAttack()
        {
            handsAnimator.SetBool("SecondaryAttack", true);
            int count = 0;

            switch (weaponType)
            {
                case WeaponType.ShotgunAxe:
                    while(count < 30)
                    {
                        if (bullets[count].gameObject.activeSelf)
                        {
                            count++;
                            continue;
                        }

                        float rng = Random.Range(-20, 20);
                        Quaternion rotation = Quaternion.Euler(transform.rotation.eulerAngles + (Vector3.forward * rng));
                        bullets[count].Fire(shotgunBarrell.position, rotation, weaponType);

                        count++;
                        if (count >= bullets.Length) break;
                    }
                    break;
                case WeaponType.Macenigun:
                    weaponAnimator.SetBool("MaceSpinning", true);

                    while(true)
                    {
                        if(!bullets[count].gameObject.activeSelf)
                        {
                            float rng = Random.Range(-3, 3);
                            Quaternion rotation = Quaternion.Euler(transform.rotation.eulerAngles + (Vector3.forward * rng));
                            bullets[count].Fire(maceBarrell.position, rotation, weaponType);

                            break;
                        }

                        count++;
                    }
                    break;
                case WeaponType.SwordLaser:
                    weaponAnimator.SetBool("SwordGleaming", true);

                    laserDuration += Time.deltaTime;
                    if (laserDuration >= laserBuildTime) laserDuration = laserBuildTime;

                    RaycastHit2D raycasthit = Physics2D.Raycast(swordBarrell.position, transform.up, laserLength * (laserDuration / laserBuildTime), ~(1 << LayerMask.NameToLayer("Player") | 
                                                                                                                                                       1 << LayerMask.NameToLayer("PlayerWeapon") | 
                                                                                                                                                       1 << LayerMask.NameToLayer("Items")));
                    Debug.DrawLine(swordBarrell.position, raycasthit.point, Color.red, 0.1f);

                    line.enabled = true;
                    line.SetPosition(0, swordBarrell.position);
                    swordParticleEnd.gameObject.SetActive(true);

                    if (raycasthit.collider != null && raycasthit.collider.GetComponent<IDamageble>() != null)
                    {
                        IDamageble other = raycasthit.collider.GetComponent<IDamageble>();
                        if (other.GetIsEnemy())
                        {
                            other.OnHit(1);
                        }

                        swordParticleEnd.transform.position = raycasthit.point;
                        line.SetPosition(1, raycasthit.point);
                    }
                    else
                    {
                        swordParticleEnd.transform.position = swordBarrell.position + (transform.up * (laserLength * (laserDuration / laserBuildTime)));
                        line.SetPosition(1, swordBarrell.position + transform.up * (laserLength * (laserDuration / laserBuildTime)));
                    }

                    break;
                case WeaponType.Hammissile:

                    while (true)
                    {
                        if (!bullets[count].gameObject.activeSelf)
                        {
                            Quaternion rotation = Quaternion.Euler(transform.rotation.eulerAngles);

                            bullets[count].Fire(hammerBarrell.position, rotation, weaponType);

                            break;
                        }

                        count++;
                    }
                    break;
            }

            if (weaponType != WeaponType.SwordLaser)
            {
                _bulletAudio.clip = bulletSfx[(int)weaponType];
                _bulletAudio.Play();
            }
            else if (!playingBeam)
            {
                playingBeam = true;
                StartCoroutine("SwordBeamSfx");
            }

            ammo--;
            bootstrap.HUD.SetAmmo(ammo / totalAmmo);
            if (ammo <= 0) ReleaseSecondaryAttack();
        }

        public override void PrimaryAttackReset()
        {
            base.PrimaryAttackReset();
            weaponCollider.enabled = false;
        }

        void ReleaseSecondaryAttack()
        {
            handsAnimator.SetBool("SecondaryAttack", false);

            switch (weaponType)
            {
                case WeaponType.Macenigun:
                    weaponAnimator.SetBool("MaceSpinning", false);
                    break;
                case WeaponType.SwordLaser:
                    playingBeam = false;
                    StopCoroutine("SwordBeamSfx");
                    weaponAnimator.SetBool("SwordGleaming", false);
                    laserDuration = 0f;
                    line.enabled = false;
                    swordParticleEnd.gameObject.SetActive(false);
                    break;
            }
        }

        IEnumerator ShotDelay()
        {
            float cooldown = 0f;

            switch(weaponType)
            {
                case WeaponType.ShotgunAxe:
                    cooldown = 1f;
                    break;
                case WeaponType.Macenigun:
                    cooldown = 0.02f;
                    break;
                case WeaponType.SwordLaser:
                    cooldown = 0f;
                    break;
                case WeaponType.Hammissile:
                    cooldown = 2f;
                    break;
            }

            yield return new WaitForSeconds(cooldown);

            isCoolingDown = false;
        }

        float GetAmmo(WeaponType type)
        {
            switch (type)
            {
                case WeaponType.ShotgunAxe:
                    return 15f;
                case WeaponType.Macenigun:
                    return 400f;
                case WeaponType.SwordLaser:
                    return 300f;
                case WeaponType.Hammissile:
                    return 5f;
                default:
                    return 15f;
            }
        }

        void SetWeaponCollider(WeaponType type)
        {
            switch (type)
            {
                case WeaponType.ShotgunAxe:
                    weaponCollider.offset = new Vector2(0.45f, 1.75f);
                    break;
                case WeaponType.Macenigun:
                    weaponCollider.offset = new Vector2(0.45f, 1.75f);
                    break;
                case WeaponType.SwordLaser:
                    weaponCollider.offset = new Vector2(0.45f, 1.75f);
                    break;
                case WeaponType.Hammissile:
                    weaponCollider.offset = new Vector2(0.45f, 2.5f);
                    break;
            }
        }

        IEnumerator SwordBeamSfx()
        {
            while(true)
            {
                _bulletAudio.clip = bulletSfx[(int)weaponType];
                _bulletAudio.Play();
                yield return new WaitForSeconds(0.3f);
            }
        }

        public override void OnHit(int value)
        {
            base.OnHit(value);

            bootstrap.HUD.SetHealth((float)hp / (float)maxHp);
        }

        void Dead()
        {
            if (deathXplosion != null)
                Instantiate(deathXplosion, transform.position, Quaternion.identity);

            inputs.active = false;

            gameObject.SetActive(false);
            bootstrap.fade.FadeInOut(false, 2f);

            ScoreController score = bootstrap.GetController(ControllerType.Score) as ScoreController;

            ScoreHolder.score = score.score;
        }

        public override void Heal(int amount)
        {
            base.Heal(amount);
            bootstrap.HUD.SetHealth((float)hp / (float)maxHp);
        }

        public void Appear()
        {
            Instantiate(portal, transform.position, Quaternion.identity);
            ShowHide();
            weaponType = WeaponType.Macenigun;
        }

        public void ShowHide()
        {
            _renderer.enabled = !_renderer.enabled;
            footAnimator.gameObject.SetActive(!footAnimator.gameObject.activeSelf);
            handsAnimator.gameObject.SetActive(!handsAnimator.gameObject.activeSelf);
        }
    }
}