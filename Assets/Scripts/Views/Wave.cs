﻿using System.Collections.Generic;
using CruzScripts.Scripts.Controller;
using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace CruzScripts.Scripts.Views
{
    public class Wave : GenericView
    {
        public float InteractionRadius;

        public WaveConfiguration Configuration;
        private EnemiesFactory factory;

        private bool waveStarted;
        public float WaveCooldown = 5;

        public override void OnAwake()
        {
            base.OnAwake();

            Configuration.TopRightLimit += new Vector2(transform.position.x, transform.position.y);
            Configuration.BottonLeftLimit = new Vector2(transform.position.x, transform.position.y)- Configuration.BottonLeftLimit;
        }

        public override void OnStart()
        {
            base.OnStart();

            factory = bootstrap.GetController(ControllerType.Factory) as EnemiesFactory;

        }

        private void WaveEnd()
        {
            factory.WaveEnded -= WaveEnd;
            Invoke("ResetWave", WaveCooldown);
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            if (!waveStarted && !factory.isWaveOpen)
                CheckInside();
        }

        void CheckInside()
        {
            if (factory.players.Exists(x => Vector3.Distance(x.transform.position, transform.position) <= InteractionRadius))
                StartWave();
        }

        void StartWave()
        {
            waveStarted = true;
            factory.StartWave(Configuration);
            factory.WaveEnded += WaveEnd;
        }

        void ResetWave()
        {
            waveStarted = false;
        }

        public void OnDrawGizmos()
        {
            if (!waveStarted)
            {
                var color = Color.green;
                Debug.DrawRay(transform.position, Vector3.up*InteractionRadius, color);
                Debug.DrawRay(transform.position, Vector3.down*InteractionRadius, color);
                Debug.DrawRay(transform.position, Vector3.left*InteractionRadius, color);
                Debug.DrawRay(transform.position, Vector3.right*InteractionRadius, color);

                color = Color.blue;
                for (int i = 0; i < Configuration.SpawnPoints.Length; i++)
                {
                    Debug.DrawRay(Configuration.SpawnPoints[i].position, Vector3.up, color);
                    Debug.DrawRay(Configuration.SpawnPoints[i].position, Vector3.down, color);
                    Debug.DrawRay(Configuration.SpawnPoints[i].position, Vector3.left, color);
                    Debug.DrawRay(Configuration.SpawnPoints[i].position, Vector3.right, color);
                }


                color = Color.red;
                Debug.DrawRay(transform.position - (Vector3)Configuration.BottonLeftLimit, Vector3.up, color);
                Debug.DrawRay(transform.position - (Vector3)Configuration.BottonLeftLimit, Vector3.right, color);
                Debug.DrawRay(transform.position + (Vector3)Configuration.TopRightLimit, Vector3.down, color);
                Debug.DrawRay(transform.position + (Vector3)Configuration.TopRightLimit, Vector3.left, color);
            }
        }
    }
}