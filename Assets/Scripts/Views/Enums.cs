﻿namespace Enums
{
    public enum WeaponType
    {
        ShotgunAxe = 0,
        Macenigun = 1,
        SwordLaser = 2,
        Hammissile = 3,
        Melee = 4,
        Pistol = 5
    }
}