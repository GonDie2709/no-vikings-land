﻿using UnityEngine;

namespace CruzScripts.Scripts.Views
{
    public class GenericView : MonoBehaviour, IView
    {
        protected Bootstrap bootstrap;
        public bool initialized { get; private set; }

        void Awake()
        {
            initialized = false;
            OnAwake();
        }
        void Start()
        {
            bootstrap = Bootstrap.Instance;
            bootstrap.Initialized += Initialize;
        }
        void Update()
        {
            if (!initialized) return;

            OnUpdate();
        }
        
        public void Initialize()
        {
            initialized = true;
            OnStart();
        }

        public virtual void OnStart()
        {
            if (bootstrap == null)
                bootstrap = Bootstrap.Instance;
        }
        public virtual void OnUpdate() { }
        public virtual void OnAwake() { }
        public virtual void Interact(IView other) { }
    }
}