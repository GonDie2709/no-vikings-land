﻿using UnityEngine;
using System.Collections;
using CruzScripts.Scripts;
using CruzScripts.Scripts.Controller;
using CruzScripts.Scripts.Views;

public class CameraFollow : GenericView
{
    private EnemiesFactory factory;
    private bool isLocked;
    public override void OnStart()
    {
        base.OnStart();

        factory = bootstrap.GetController(ControllerType.Factory) as EnemiesFactory;

        factory.WaveStarted += LockCamera;
        factory.WaveEnded += UnlockCamera;
    }

    private void UnlockCamera()
    {
        isLocked = false;
    }

    private void LockCamera()
    {
        isLocked = true;
    }

    public float Speed = 4;
    public override void OnUpdate ()
    {
        var finalPos = (-transform.position + (bootstrap.player.transform.position + (Vector3.back * 20f))) * Speed * Time.deltaTime;
        
        if (isLocked)
        {
            var endPos = transform.position + (-transform.position + (bootstrap.player.transform.position + (Vector3.back * 20f))) * Speed * Time.deltaTime;

            var count = 0;
            if (endPos.x < factory.actualConfig.BottonLeftLimit.x || endPos.x > factory.actualConfig.TopRightLimit.x)
            {
                count++;
                finalPos.x = 0;
            }
            if (endPos.y < factory.actualConfig.BottonLeftLimit.y || endPos.y > factory.actualConfig.TopRightLimit.y)
            {
                count++;
                finalPos.y = 0;
            }
            if (count >= 2) return;
        }

        transform.position += finalPos;
    }
}