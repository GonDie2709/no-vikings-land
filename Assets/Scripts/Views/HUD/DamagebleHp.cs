﻿using UnityEngine;
using UnityEngine.UI;

namespace CruzScripts.Scripts.Views.HUD
{
    public class DamagebleHp : GenericView
    {
        public Image image;

        [HideInInspector]
        public IDamageble owner;

        [HideInInspector] public Vector3 offset;

        public override void OnStart()
        {
            base.OnStart();

            transform.position = owner.GetGameObject().transform.position + offset;
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            transform.position = owner.GetGameObject().transform.position + offset;

            image.fillAmount = (float)((float)owner.GetHP()/(float)owner.GetMaxHP());
        }
    }
}