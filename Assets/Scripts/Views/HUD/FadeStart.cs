﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CruzScripts.Scripts.Views;
using DG.Tweening;
using System;
using UnityEngine.SceneManagement;

public class FadeStart : MonoBehaviour
{
    Image _image;
    public string sceneTo;

	// Use this for initialization
	void Awake ()
    {
	    _image = GetComponent<Image>();
	}

    void Start()
    {
        FadeInOut(true, 0f);
    }

    public void FadeInOut(bool fadeIn, float delay)
    {
        if(fadeIn)
            _image.DOFade(0f, 0.5f).SetDelay(delay).Play();
        else
            _image.DOFade(1f, 1.5f).SetDelay(delay).Play().OnComplete(ChangeScene);
    }

    void ChangeScene()
    {
        SceneManager.LoadScene(sceneTo);
    }
}