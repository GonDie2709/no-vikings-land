﻿using UnityEngine;
using System.Collections;
using CruzScripts.Scripts.Views;
using CruzScripts.Scripts.Views.HUD;
using UnityEngine.UI;

public class HUD : GenericView, IHUD
{
    Image health, ammo;
    float healthFrom, healthTo;
    public Text counting;

    Coroutine healthRoutine;

    // Use this for initialization
	public override void OnStart ()
    {
        base.OnStart();

        bootstrap.HUD = this;
        health = transform.FindChild("Health").GetComponent<Image>();
        ammo = transform.FindChild("Ammo").GetComponent<Image>();
        counting = transform.FindChild("Count").GetComponent<Text>();
    }
	
    public void SetAmmo(float amount)
    {
        ammo.fillAmount = amount;
    }

    public void SetHealth(float amount)
    {
        healthFrom = health.fillAmount;
        healthTo = amount;

        if (healthRoutine != null) StopCoroutine(healthRoutine);
        healthRoutine = StartCoroutine(LerpHealthBar());
    }

    IEnumerator LerpHealthBar()
    {
        float timer = 0f;

        while(true)
        {
            timer += Time.deltaTime;
            health.fillAmount = Mathf.Lerp(healthFrom, healthTo, timer / 0.5f);

            yield return null;
        }
    }
}