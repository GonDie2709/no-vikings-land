﻿using CruzScripts.Scripts.Controller;
using CruzScripts.Scripts.Views.Creature;

namespace CruzScripts.Scripts.Views.HUD
{
    public class ScoreText : TextView
    {
        protected ScoreController scoreController;
        public override void OnStart()
        {
            base.OnStart();

            scoreController = bootstrap.GetController(ControllerType.Score) as ScoreController;
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            text.text = scoreController.score.ToString("000000");
        }
    }
}