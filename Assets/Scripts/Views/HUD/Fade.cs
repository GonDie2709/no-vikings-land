﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CruzScripts.Scripts.Views;
using DG.Tweening;
using System;

public class Fade : GenericView
{
    Image _image;

	// Use this for initialization
	public override void OnAwake ()
    {
	    _image = GetComponent<Image>();
	}

    public override void OnStart()
    {
        base.OnStart();

        bootstrap.fade = this;
    }

    public void FadeInOut(bool fadeIn, float delay)
    {
        _image.DOFade(fadeIn ? 0f : 1f, 1.5f).SetDelay(delay).Play().OnComplete(() => bootstrap.FadeEnded(!fadeIn));
    }
}