﻿using UnityEngine;
using UnityEngine.UI;

namespace CruzScripts.Scripts.Views.HUD
{
    [RequireComponent(typeof(Text))]
    public class TextView : GenericView
    {
        protected Text text;

        public override void OnStart()
        {
            base.OnStart();

            text = GetComponent<Text>();
        }
    }
}