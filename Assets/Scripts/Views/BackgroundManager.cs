﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using CruzScripts.Scripts.Views;

public class BackgroundManager : GenericView
{
    //public GameObject background;

    static BackgroundManager _instance;
    public static BackgroundManager instance { get { return _instance; } }

    public BackgroundInstance[] backgrounds;
    BackgroundInstance _current;
    Vector3 _bgSize;
    Transform _player;

    // Use this for initialization
    public override void OnStart()
    {
        _instance = this;
        _player = bootstrap.player.GetComponent<Transform>();

        //background.transform.rotation = Quaternion.identity;
        //backgrounds = new BackgroundInstance[9];
        //backgrounds[0] = new BackgroundInstance(background);

        _current = backgrounds[0];
        _current.used = true;

        _bgSize = backgrounds[0].renderer.bounds.size - (Vector3.one * 0.01f);

        /*for (int i = 1; i <= backgrounds.Length - 1; i++)
        {
            GameObject newBg = Instantiate(background) as GameObject;
            newBg.transform.parent = background.transform.parent;

            newBg.transform.position = _current.GetPosition();
            newBg.transform.rotation = Quaternion.identity;
            backgrounds[i] = new BackgroundInstance(newBg);
        }*/

        UpdateBackground(_current);
    }

    void Update()
    {
        BackgroundInstance closest = ClosestBackground();
        if(_current != closest)
        {
            UpdateBackground(closest);
        }
    }

    public void UpdateBackground(BackgroundInstance background)
    {
        _current = background;

        Vector3 startingPos;
        startingPos.x = _current.GetPosition().x - _bgSize.x;
        startingPos.y = _current.GetPosition().y + _bgSize.y;
        startingPos.z = 0;
        Vector3 newPos = Vector3.zero;

        for(int y = 0; y < 3; y++)
        {
            newPos.y = startingPos.y - (_bgSize.y * y);
            for (int x = 0; x < 3; x++)
            {
                if (y == 1 && x == 1) continue;

                newPos.x = startingPos.x + (_bgSize.x * x);

                BackgroundInstance nextBg = GetNextBackground();
                nextBg.SetPosition(newPos);
            }
        }

        for (int i = 0; i < backgrounds.Length; i++)
        {
            backgrounds[i].used = false;
        }
    }

    BackgroundInstance GetNextBackground()
    {
        for(int i = 0; i < backgrounds.Length; i++)
        {
            if (backgrounds[i] != _current && !backgrounds[i].used)
            {
                backgrounds[i].used = true;
                return backgrounds[i];
            }
        }

        return null;
    }

    BackgroundInstance GetBackground(GameObject background)
    {
        for (int i = 0; i < backgrounds.Length; i++)
        {
            if (backgrounds[i].background == background)
            {
                return backgrounds[i];
            }
        }

        return null;
    }

    BackgroundInstance ClosestBackground()
    {
        BackgroundInstance closest = backgrounds[0];
        if (_player != null)
        {
            for (int i = 1; i < backgrounds.Length; i++)
            {
                if (Vector3.Distance(_player.position, backgrounds[i].GetPosition()) < Vector3.Distance(_player.position, closest.GetPosition()))
                    closest = backgrounds[i];
            }
        }

        return closest;
    }
}