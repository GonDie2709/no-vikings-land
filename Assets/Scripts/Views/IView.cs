﻿namespace CruzScripts.Scripts.Views
{
    public interface IView
    {
        bool initialized { get; }
        void Initialize();

        void OnStart();
        void OnUpdate();
        void OnAwake();

        void Interact(IView other);
    }
}