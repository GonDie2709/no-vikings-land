﻿using CruzScripts.Scripts.Views;

namespace Assets.Scripts.Views
{
    public interface IDropable
    {
        void Drop();
    }
}