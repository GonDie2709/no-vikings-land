﻿using UnityEngine;
using System.Collections;

public class MouseFollow : MonoBehaviour
{
    void Awake()
    {
        Cursor.visible = false;
    }

	void Update ()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = transform.position.z;

        transform.position = mousePos;
	}
}