﻿using UnityEngine;
using System.Collections;
using CruzScripts.Scripts.Views;
using CruzScripts.Scripts.Views.Creature;
using Enums;

public class Projectile : MonoBehaviour
{
    Rigidbody2D _rigidbody;
    SpriteRenderer _image;
    CircleCollider2D _circle;

    public float speed = 10f;
    public float volatileSpeed;
    float duration = 0f;
    float travelTime = 0f;
    int damage;

    [HideInInspector]
    public bool IsEnemy;

    WeaponType bulletType;

    public Sprite[] bulletImg;

    bool isSet = false;
    Coroutine selfDestroy;

    AudioSource _audio;
    Animator explosionAnimator;

    void Start()
    {
        _circle = GetComponent<CircleCollider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _image = GetComponent<SpriteRenderer>();

        _audio = GetComponent<AudioSource>();

        explosionAnimator = transform.FindChild("HitSpark").GetComponent<Animator>();
    }

    private bool hited;
    // Update is called once per frame
    void Update()
    {
        if (hited) return;

        travelTime += Time.deltaTime;

        _rigidbody.velocity = transform.up * volatileSpeed * Time.deltaTime;

        if(bulletType != WeaponType.Hammissile)
            transform.localScale = Vector3.Lerp(Vector3.one, new Vector3(0f, 1f, 1f), travelTime / duration);
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<IDamageble>() != null)
        {
            if (collision.gameObject.GetComponent<IDamageble>().GetIsEnemy() == IsEnemy)
            {
                //collision.gameObject.GetComponent<IDamageble>().OnHit(damage);
            }
            else
            {
                _audio.Play();

                collision.gameObject.GetComponent<IDamageble>().OnHit(damage);
                
                explosionAnimator.Play("HitSpark");

                if (bulletType != WeaponType.Hammissile)
                {
                    Invoke("DisableSelf", 1f);

                    hited = true;
                    _rigidbody.velocity = Vector3.zero;
                    transform.localScale = Vector3.one;
                    GetComponent<SpriteRenderer>().enabled = false;
                }
            }
        }
    }

    void DisableSelf()
    {
        hited = false;
        gameObject.SetActive(false);
        GetComponent<SpriteRenderer>().enabled = true;
    }

    public void Fire(Vector3 position, Quaternion rotation, WeaponType type)
    {
        IsEnemy = false;
        if (selfDestroy != null)
            StopCoroutine(selfDestroy);

        if(!isSet)
        {
            isSet = true;

            _circle = GetComponent<CircleCollider2D>();
            _rigidbody = GetComponent<Rigidbody2D>();
            _image = GetComponent<SpriteRenderer>();
        }

        bulletType = type;

        travelTime = 0f;
        transform.position = position;
        transform.rotation = rotation;
        transform.localScale = Vector3.one;

        volatileSpeed = speed + Random.Range(0f, speed);

        SetBulletInfo(type);
        GetComponent<SpriteRenderer>().sprite = GetImage(type);

        gameObject.SetActive(true);
        selfDestroy = StartCoroutine(TimedSelfDestroy());
    }

    public void Fire(Vector3 position, Quaternion rotation, WeaponType type, float duration, int damage)
    {
        IsEnemy = true;
        if (selfDestroy != null)
            StopCoroutine(selfDestroy);

        bulletType = type;

        travelTime = 0f;
        transform.position = position;
        transform.rotation = rotation;

        volatileSpeed = speed + Random.Range(0f, speed);

        this.damage = damage;
        this.duration = duration;
        GetComponent<SpriteRenderer>().sprite = GetImage(type);

        gameObject.SetActive(true);
        selfDestroy = StartCoroutine(TimedSelfDestroy());
    }

    IEnumerator TimedSelfDestroy()
    {
        yield return new WaitForSeconds(duration);

        gameObject.SetActive(false);
    }

    void SetBulletInfo(WeaponType type)
    {
        _circle.radius = 0.23f;

        switch (type)
        {
            case WeaponType.ShotgunAxe:
                duration = 0.4f;
                damage = 2;
                break;
            case WeaponType.Macenigun:
                duration = 1f;
                damage = 1;
                break;
            case WeaponType.SwordLaser:
                duration = 3f;
                damage = 4;
                break;
            case WeaponType.Hammissile:
                _circle.radius = 0.6f;
                duration = 3f;
                damage = 20;
                break;
            default:
                duration = 0f;
                damage = 0;
                break;
        }
    }

    Sprite GetImage(WeaponType type)
    {
        return bulletImg[(int)type];
    }
}
