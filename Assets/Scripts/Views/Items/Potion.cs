﻿using UnityEngine;
using System.Collections;
using CruzScripts.Scripts.Views;
using CruzScripts.Scripts.Views.Creature;

public class Potion : BaseItem
{
    public int healAmount = 5;

    public override void OnCollect(IView other)
    {
        Player player = other as Player;

        if (player.CompareTag("Player"))
        {
            PlayAudio();

            player.Heal(healAmount);
            HideAndDestroy();
        }
    }
}