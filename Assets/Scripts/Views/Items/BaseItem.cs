﻿using UnityEngine;
using System.Collections;
using CruzScripts.Scripts.Views;
using CruzScripts.Scripts.Views.Creature;

public class BaseItem : GenericView, ICollectable
{
    protected AudioSource _audio;
    protected Collider2D _collider;
    protected SpriteRenderer _renderer;

    public AudioClip pickupSfx;

    public override void OnStart()
    {
        base.OnStart();

        _audio = GetComponent<AudioSource>();
        _collider = GetComponent<Collider2D>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    public virtual void OnCollect(IView Other) {  }

    protected void HideAndDestroy()
    {
        _renderer.enabled = false;
        _collider.enabled = false;
        Invoke("SelfDestroy", 3f);
    }

    void SelfDestroy()
    {
        Destroy(gameObject);
    }

    protected void PlayAudio()
    {
        if (_audio)
        {
            _audio.clip = pickupSfx;
            _audio.Play();
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.GetComponent<Player>())
            OnCollect(col.gameObject.GetComponent<IView>());
    }
}