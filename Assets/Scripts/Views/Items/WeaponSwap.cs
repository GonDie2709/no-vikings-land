﻿using UnityEngine;
using System.Collections;
using Enums;
using CruzScripts.Scripts.Views;
using CruzScripts.Scripts.Views.Creature;

public class WeaponSwap : BaseItem
{
    public WeaponType changeTo;
    public Sprite[] weaponImgs;

    public override void OnStart()
    {
        base.OnStart();

        _renderer.sprite = weaponImgs[(int)changeTo];
    }

    public override void OnCollect(IView other)
    {
        Player player = other as Player;

        if (player.CompareTag("Player"))
        {
            PlayAudio();

            player.weaponType = changeTo;
            HideAndDestroy();
        }
    }
}
