﻿using System.Collections;
using System.Collections.Generic;
using CruzScripts.Scripts.Controller;
using CruzScripts.Scripts.Models;
using CruzScripts.Scripts.Service;
using UnityEngine;
using CruzScripts.Scripts.Views.Creature;
using UnityEngine.SceneManagement;

namespace CruzScripts.Scripts
{
    public class Bootstrap : MonoBehaviour, IBootstrap
    {
        public delegate void SimpleEvent();
        public delegate void intEvent(int value);
        public delegate void floatEvent(float value);
        public delegate void boolEvent(bool value);
        public delegate void Vector2Event(Vector2 value);
        public delegate void Vector3Event(Vector3 value);

        public static Bootstrap Instance;
        public Player player;
        public HUD HUD;
        public Fade fade;

        public event SimpleEvent Initialized;
        public List<IController> Controllers { get; private set; }
        public List<IModel> Models { get; private set; }
        public List<IService> Services { get; private set; }

        public event boolEvent FadeEnd;

        void Awake()
        {
            if (Bootstrap.Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Bootstrap.Instance = this;
            }

            Controllers = new List<IController>();
            Models = new List<IModel>();
            Services = new List<IService>();

            FadeEnd += ResetGame;
        }
        void Start()
        {
            StartCoroutine(StartRoutine());
        }
        IEnumerator StartRoutine()
        {
            yield return null;
            Initialize();
            
            yield return null;
            for (int i = 0; i < Controllers.Count; i++) { Controllers[i].Initialize(); }
            
            yield return null;
            for (int i = 0; i < Models.Count; i++) { Models[i].Initialize(); }

            yield return null;
            for (int i = 0; i < Services.Count; i++) { Services[i].Initialize(); }

            if (Initialized != null) Initialized();

            FadeGameIn();
        }
        
        public void FadeGameIn()
        {
            if(fade != null)
                fade.FadeInOut(true, 0f);
        }

        public void FadeEnded(bool endGame)
        {
            if (FadeEnd != null) FadeEnd(endGame);
        }

        public void ResetGame(bool endGame)
        {
            if (endGame)
                SceneManager.LoadScene("Credits");
            else
                StartCoroutine(FreeInputs());
        }

        IEnumerator FreeInputs()
        {
            int count = 3;
            
            player.Appear();

            yield return new WaitForSeconds(0.5f);

            HUD.counting.color = Color.white;

            while (count >= 0)
            {
                if(count > 0)
                    HUD.counting.text = count.ToString();
                else
                    HUD.counting.text = "Go!";

                count--;

                yield return new WaitForSeconds(1f);
            }

            HUD.counting.gameObject.SetActive(false);

            InputController input = GetController(ControllerType.Input) as InputController;
            input.active = true;
        }

        #region Interface Methods
        public void Initialize()
        {
        }
        public void SetController(IController value)
        {
            if (Controllers.Exists(x => x.Type == value.Type))
            {
                throw new System.AccessViolationException();
            }
            else
            {
                Debug.Log(value + " Added");
                Controllers.Add(value);
            }
        }
        public IController GetController(ControllerType type)
        {
            if (Controllers.Exists(x => x.Type == type))
                return Controllers.Find(x => x.Type == type);
            else
            {
                Debug.LogError("Not Found");
                return null;
            }
        }
        public void SetModel(IModel value)
        {
            if (Models.Exists(x => x.Type == value.Type))
            {
                throw new System.AccessViolationException();
            }
            else
                Models.Add(value);
        }
        public IModel Getmodel(ModelType type)
        {
            if (Models.Exists(x => x.Type == type))
                return Models.Find(x => x.Type == type);
            else
                return null;
        }
        public void SetService(IService value)
        {
            if (Services.Exists(x => x.Type == value.Type))
            {
                throw new System.AccessViolationException();
            }
            else
                Services.Add(value);
        }
        public IService GetSerice(ServiceType type)
        {
            if (Services.Exists(x => x.Type == type))
                return Services.Find(x => x.Type == type);
            else
                return null;
        }
        #endregion
    }
}