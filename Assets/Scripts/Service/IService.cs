﻿namespace CruzScripts.Scripts.Service
{
    public interface IService
    {
        ServiceType Type { get; }
        void Initialize();
        void OnStart();
        void OnUpdate();
        void OnAwake();
        void Connect();
        void Disconnect();
    }
}