﻿using DG.Tweening;

namespace CruzScripts.Scripts.Controller
{
    public class ScoreController : GenericController
    {
        public int score;
        public int realScore;

        void Awake()
        {
            Type = ControllerType.Score;
        }
        public void AddScore(int value)
        {
            realScore += value;

            DOTween.To(x => score = (int)x, score, realScore, 0.4f).SetEase(Ease.OutQuad).Play();
        }
    }
}