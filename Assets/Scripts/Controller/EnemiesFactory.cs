﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using CruzScripts.Scripts.Views;
using CruzScripts.Scripts.Views.Creature;
using Enums;
using UnityEngine;

namespace CruzScripts.Scripts.Controller
{
    public enum EnemyType
    {
        MeleeMinion,
        RangedMinion,
        Suicide,
        Tank,
        Dash,
        Druid
    }
    public class EnemiesFactory : GenericController
    {
        public List<EnemySource> EnemySources;
        public List<BaseItem> CollectablesReference;
        public List<EnemyPool> EnemiesPool;

        [Space]
        public Transform RootParent;
        public Vector3 voidPosition = new Vector3(-999,-999,0);

        [HideInInspector]
        public List<CreatureBase> players; 

        public event Bootstrap.SimpleEvent WaveStarted, WaveEnded;

        private bool canInstance;
        private int deathCount;

        [HideInInspector]
        public WaveConfiguration actualConfig;
        [HideInInspector]
        public bool isWaveOpen;

        void Awake()
        {
            Type = ControllerType.Factory;
        }

        public override void OnStart()
        {
            base.OnStart();

            EnemiesPool = new List<EnemyPool>();

            var enu = Enum.GetNames(typeof (EnemyType));
            for (int i = 0; i < enu.Length; i++)
            {
                EnemiesPool.Add(new EnemyPool() {pool = new List<CreatureBase>(), type = (EnemyType) i});
            }

            players = GameObject.FindObjectsOfType<CreatureBase>().ToList().FindAll(x => !x.isEnemy);

            FillMemory();
        }

        void FillMemory()
        {
            InstantiateEnemy(EnemyType.MeleeMinion, false);
            InstantiateEnemy(EnemyType.MeleeMinion, false);
            InstantiateEnemy(EnemyType.MeleeMinion, false);
            InstantiateEnemy(EnemyType.MeleeMinion, false);

            InstantiateEnemy(EnemyType.RangedMinion, false);
            InstantiateEnemy(EnemyType.RangedMinion, false);
            InstantiateEnemy(EnemyType.RangedMinion, false);
            InstantiateEnemy(EnemyType.RangedMinion, false);

            InstantiateEnemy(EnemyType.Dash, false);
            InstantiateEnemy(EnemyType.Dash, false);

            InstantiateEnemy(EnemyType.Suicide, false);
            InstantiateEnemy(EnemyType.Suicide, false);
            InstantiateEnemy(EnemyType.Suicide, false);
            InstantiateEnemy(EnemyType.Suicide, false);

            InstantiateEnemy(EnemyType.Druid, false);
            InstantiateEnemy(EnemyType.Druid, false);
            InstantiateEnemy(EnemyType.Druid, false);
            InstantiateEnemy(EnemyType.Druid, false);
            InstantiateEnemy(EnemyType.Druid, false);
            InstantiateEnemy(EnemyType.Druid, false);

            InstantiateEnemy(EnemyType.Tank, false);
            InstantiateEnemy(EnemyType.Tank, false);
        }

        public void StartWave(WaveConfiguration config)
        {
            if (isWaveOpen) return;

            Debug.Log("Start Wave");

            actualConfig = config;
            isWaveOpen = true;
            canInstance = false;
            deathCount = 0;

            if (WaveStarted != null) WaveStarted();

            StartCoroutine(StartWaveAsync(config));

            return;
        }

        void SpawnEnemies(WaveConfiguration config)
        {
            canInstance = false;
            var rnd = new System.Random();

            var last = 0;
            var index = rnd.Next(0, config.SpawnPoints.Length);
            var pos = config.SpawnPoints[index];

            for (int i = 0; i < config.qntEnemiesMaxAtOnce; i++)
            {
                var temp = InstantiateEnemy(config.types[rnd.Next(0, config.types.Count)], false);

                if (i > 0 && config.SpawnPoints.Length > 1)
                {
                    index = rnd.Next(0, config.SpawnPoints.Length);
                    while (last == index)
                    {
                        index = rnd.Next(0, config.SpawnPoints.Length);
                    }
                    pos = config.SpawnPoints[index];
                }
                temp.Spawn(pos.position);

                temp.DiedCreature += WaveEnemyDied;

                last = index;
            }
            CheckDethCount();
        }

        IEnumerator StartWaveAsync(WaveConfiguration config)
        {
            canInstance = false;
            var rnd = new System.Random();

            var last = 0;
            var index = rnd.Next(0, config.SpawnPoints.Length);
            var pos = config.SpawnPoints[index];

            for (int i = 0; i < config.qntEnemiesMaxAtOnce; i++)
            {
                var temp = InstantiateEnemy(config.types[rnd.Next(0, config.types.Count)], false);

                if (i > 0 && config.SpawnPoints.Length>1)
                {
                    index = rnd.Next(0, config.SpawnPoints.Length);
                    while (last == index)
                    {
                        index = rnd.Next(0, config.SpawnPoints.Length);
                    }
                    pos = config.SpawnPoints[index];
                }

                if (CollectablesReference.Count > 0)
                    temp.DroppableItem = CollectablesReference[rnd.Next(0, CollectablesReference.Count)];

                temp.Spawn(pos.position);

                temp.DiedCreature += WaveEnemyDied;

                last = index;
                yield return null;
            }
            CheckDethCount();
        }

        void CheckDethCount()
        {
            if(!isWaveOpen || actualConfig == null) return;

            if (deathCount >= actualConfig.qntEnemiesTotal)
            {
                EndtWave();
            }
            else if (canInstance && deathCount > 0 && deathCount % actualConfig.qntEnemiesMaxAtOnce == 0)
            {
                SpawnEnemies(actualConfig);
            }
        }

        private void WaveEnemyDied(CreatureBase obj)
        {
            obj.DiedCreature -= WaveEnemyDied;
            deathCount++;
            canInstance = true;
            CheckDethCount();
        }

        public void EndtWave()
        {
            Debug.Log("EndtWave");

            actualConfig = null;
            isWaveOpen = false;
            if (WaveEnded != null) WaveEnded();
        }

        public CreatureBase InstantiateEnemy(EnemyType type, bool forceInstance)
        {
            if (!forceInstance && EnemiesPool.Exists(x => x.type == type && x.pool.FindAll(y => y.isDead).Count > 0))
            {
                var pool = EnemiesPool.Find(x => x.type == type).pool.Find(y => y.isDead);
                return pool;
            }

            var rnd = new System.Random();
            var types = EnemySources.FindAll(x => x.type == type);
            var temp = Instantiate(Resources.Load<CreatureBase>(types[rnd.Next(0, types.Count)].rescouceName));

            EnemiesPool.Find(x=>x.type==type).pool.Add(temp);
            temp.transform.parent = RootParent;
            temp.transform.position = voidPosition;
            temp.Initialize();
            return temp;
        }
    }

    [System.Serializable]
    public class EnemySource
    {
        public string rescouceName;
        public EnemyType type;
    }
    public class EnemyPool
    {
        public EnemyType type;
        public List<CreatureBase> pool;
    }
    [System.Serializable]
    public class WaveConfiguration
    {
        public int qntEnemiesMaxAtOnce;
        public int qntEnemiesTotal;
        public List<EnemyType> types;
        public Transform[] SpawnPoints;
        public Vector2 TopRightLimit, BottonLeftLimit;
    }
}