﻿using CruzScripts.Scripts.Views;
using CruzScripts.Scripts.Views.HUD;
using UnityEngine;

namespace CruzScripts.Scripts.Controller
{
    public class WorldCanvasController : GenericController
    {
        void Awake()
        {
            Type = ControllerType.WorldCanvas;
        }

        public void InstantiateHelthBar(string resourceName, IDamageble owner, Vector3 offset)
        {
            var temp = Instantiate(Resources.Load<DamagebleHp>(resourceName));
            temp.transform.parent = transform;
            temp.transform.localScale = Vector3.one;
            temp.owner = owner;
            temp.offset = offset;
            temp.Initialize();
        }
    }
}