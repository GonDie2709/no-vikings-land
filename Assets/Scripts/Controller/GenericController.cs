﻿using UnityEngine;

namespace CruzScripts.Scripts.Controller
{
    public class GenericController : MonoBehaviour, IController
    {
        public ControllerType Type { get; protected set; }
        public bool isRunning { get; private set; }

        public event Bootstrap.SimpleEvent ControllerStarted , ControllerStoped;

        protected IBootstrap bootstrap;

        void Start()
        {
            bootstrap = Bootstrap.Instance;
            bootstrap.SetController(this);
        }
        void Update()
        {
            OnUpdate();
        }
        public void Initialize()
        {
            isRunning = false;
            OnStart();
        }

        public virtual void StartController()
        {
            isRunning = true;
            if (ControllerStarted != null) ControllerStarted();
        }
        public virtual void StopController()
        {
            isRunning = false;
            if (ControllerStoped != null) ControllerStoped();
        }

        public virtual void OnStart() { }
        public virtual void OnUpdate() { }
    }
}