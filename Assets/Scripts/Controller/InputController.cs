﻿using UnityEngine;

namespace CruzScripts.Scripts.Controller
{
    public class InputController : GenericController
    {
        public bool active = false;
        public Vector2 MainAxis;
        public Vector2 SecondaryAxis;
        public bool Jump;
        public bool Fire;
        public bool Fire2;

        public event Bootstrap.SimpleEvent 
            OnJumpPress ,   OnJumpRelease ,
            OnActionPress , OnActionRelease ,
            OnFirePress ,   OnFireRelease,
            OnFire2Press, OnFire2Release,
            OnLockOnPress , OnLockOnRelease;

        void Awake()
        {
            Type = ControllerType.Input;
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            if (active)
            {
                GetAxis();
                GetButtons();
            }
        }

        #region Input reference methods
        void GetAxis()
        {
            MainAxis        = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            SecondaryAxis   = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
        void GetButtons()
        {
            Jump = Input.GetButton("Jump");
            Fire = Input.GetButton("Fire");
            Fire2 = Input.GetButton("Fire2");

            if (Input.GetButtonDown("Jump"))
                if (OnJumpPress != null) OnJumpPress();
            if (Input.GetButtonUp("Jump"))
                if (OnJumpRelease != null) OnJumpRelease();

            if (Input.GetButton("Fire"))
                if (OnFirePress != null) OnFirePress();
            if (Input.GetButtonUp("Fire"))
                if (OnFireRelease != null) OnFireRelease();

            if (Input.GetButton("Fire2"))
                if (OnFire2Press != null) OnFire2Press();
            if (Input.GetButtonUp("Fire2"))
                if (OnFire2Release != null) OnFire2Release();
        }
        #endregion
    }
}