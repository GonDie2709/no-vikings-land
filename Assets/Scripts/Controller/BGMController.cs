﻿using UnityEngine;
using System.Collections;
using CruzScripts.Scripts.Controller;

public class BGMController : GenericController
{
    public AudioClip[] musicList;
    AudioSource audio;

    int index = 0;

    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();

        index = Random.Range(0, musicList.Length - 1);

        PlayNextSong();
    }

    void PlayNextSong()
    {
        index++;
        if (index > musicList.Length - 1) index = 0;

        audio.clip = musicList[index];
        audio.Play();
        Invoke("PlayNextSong", audio.clip.length);
    }
}