﻿namespace CruzScripts.Scripts.Controller
{
    public interface IController
    {
        ControllerType Type { get;}
        bool isRunning { get; }

        void Initialize();
        void StartController();
        void StopController();
        void OnStart();
        void OnUpdate();
    }
}