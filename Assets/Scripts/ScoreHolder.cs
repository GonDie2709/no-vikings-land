﻿using UnityEngine;
using System.Collections;

public class ScoreHolder : MonoBehaviour
{
    public static ScoreHolder Instance { get; private set; }
    public static float score = 0f;

    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
}