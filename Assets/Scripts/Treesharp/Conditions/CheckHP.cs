﻿using InterativaSystem.Treesharp.Actions;

namespace InterativaSystem.Treesharp.Conditions
{
    public class CheckHP : Condition
    {
        public int lowLimit;

        public CheckHP() { }

        public CheckHP(int lowHp)
        {
            lowLimit = lowHp;
        }
        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("hp", tick.tree.id).value == null) return NodeStates.Failure;

            var hp = (int)tick.blackboard.Get("hp", tick.tree.id).value;

            if (hp <= lowLimit)
            {
                return NodeStates.Success;
            }
            else
            {
                return NodeStates.Failure;
            }
        }
    }
}