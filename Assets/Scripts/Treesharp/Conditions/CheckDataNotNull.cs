﻿namespace InterativaSystem.Treesharp.Conditions
{
    public class CheckDataNotNull : Condition
    {
        public string dataname;

        public CheckDataNotNull(string dataname)
        {
            this.dataname = dataname;
        }

        public override NodeStates Tick(Tick tick)
        {
            if (tick.blackboard.Get(dataname, tick.tree.id).value == null) return NodeStates.Failure;

            return NodeStates.Success;
        }
    }
}