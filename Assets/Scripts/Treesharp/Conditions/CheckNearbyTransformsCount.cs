﻿using System.Linq;
using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace InterativaSystem.Treesharp.Conditions
{
    public class CheckNearbyTransformsCount : Condition
    {
        public bool isEnemy;
        public int minCount;
        public float distance;

        public CheckNearbyTransformsCount(bool isEnemy,int minCount, float distance)
        {
            this.isEnemy = isEnemy;
            this.minCount = minCount;
            this.distance = distance;
        }

        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("transform", tick.tree.id).value == null) return NodeStates.Failure;

            var transform = (Transform)tick.blackboard.Get("transform", tick.tree.id).value;
            var thisCreature = (CreatureBase)tick.blackboard.Get("thisCreatureBase", tick.tree.id).value;

            var targets = GameObject.FindObjectsOfType<CreatureBase>().ToList();

            var enemies = targets.FindAll(x => x.isEnemy == !isEnemy && x != thisCreature && Vector3.Distance(transform.position, x.transform.position) <= distance);
            var min = enemies.Min(x => Vector3.Distance(x.transform.position, transform.position));

            if (enemies.Count >= minCount)
            {
                return NodeStates.Success;
            }
            else
            {
                return NodeStates.Failure;
            }
        }
    }
}