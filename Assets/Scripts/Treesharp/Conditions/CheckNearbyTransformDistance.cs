﻿using System.Linq;
using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace InterativaSystem.Treesharp.Conditions
{
    public class CheckNearbyTransformDistance : Condition
    {
        public bool isEnemy;
        public float distance;

        public CheckNearbyTransformDistance(bool isEnemy, float distance)
        {
            this.isEnemy = isEnemy;
            this.distance = distance;
        }

        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("transform", tick.tree.id).value == null) return NodeStates.Failure;

            var transform = (Transform)tick.blackboard.Get("transform", tick.tree.id).value;
            var thisCreature = (CreatureBase)tick.blackboard.Get("thisCreatureBase", tick.tree.id).value;

            var targets = GameObject.FindObjectsOfType<CreatureBase>().ToList();

            var enemies = targets.FindAll(x => x.isEnemy == !isEnemy && x != thisCreature && Vector3.Distance(transform.position, x.transform.position) <= distance);

            if (enemies.Count <= 0) return NodeStates.Failure;

            return NodeStates.Success;
        }
    }
}