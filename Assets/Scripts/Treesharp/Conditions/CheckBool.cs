﻿namespace InterativaSystem.Treesharp.Conditions
{
    public class CheckBool : Condition
    {
        public string dataname;

        public CheckBool(string dataname)
        {
            this.dataname = dataname;
        }

        public override NodeStates Tick(Tick tick)
        {
            if (tick.blackboard.Get(dataname, tick.tree.id).value == null) return NodeStates.Failure;
            var chk = (bool) tick.blackboard.Get(dataname, tick.tree.id).value;

            if (chk)
                return NodeStates.Success;

            return NodeStates.Failure;
        }
    }
}