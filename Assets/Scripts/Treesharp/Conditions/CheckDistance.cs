﻿using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace InterativaSystem.Treesharp.Conditions
{
    public class CheckDistance : Condition
    {
        public float distance;
        public string targetName;

        public CheckDistance(float distance, string targetName)
        {
            this.distance = distance;
            this.targetName = targetName;
        }

        public override NodeStates Tick(Tick tick)
        {
            if (tick.blackboard.Get("transform", tick.tree.id).value == null) return NodeStates.Failure;
            if (tick.blackboard.Get(targetName, tick.tree.id).value == null) return NodeStates.Failure;

            var transform = (Transform)tick.blackboard.Get("transform", tick.tree.id).value;
            var target = (CreatureBase)tick.blackboard.Get(targetName, tick.tree.id).value;

            if (Vector3.Distance(transform.position, target.transform.position) <= distance)
            {
                return NodeStates.Success;
            }
            else
            {
                return NodeStates.Failure;
            }
        }
    }
}