﻿namespace InterativaSystem.Treesharp.Conditions
{
    public class CheckIsDead : Condition
    {
        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("isDead", tick.tree.id).value == null) return NodeStates.Failure;

            var isDead = (bool)tick.blackboard.Get("isDead", tick.tree.id).value;

            if (isDead)
            {
                return NodeStates.Failure;
            }
            else
            {
                return NodeStates.Success;
            }
        }
    }
}