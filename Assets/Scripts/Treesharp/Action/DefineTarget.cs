﻿using System.Collections.Generic;
using System.Linq;
using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace InterativaSystem.Treesharp.Actions
{
    public class DefineTarget : Action
    {
        public float distance;
        public bool isEnemy;
        public DefineTarget(bool isEnemy)
        {
            this.isEnemy = isEnemy;
            distance = 100;
        }
        public DefineTarget(float distance, bool isEnemy)
        {
            this.isEnemy = isEnemy;
            this.distance = distance;
        }

        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("transform", tick.tree.id).value == null) return NodeStates.Failure;
            if (tick.blackboard.Get("thisCreatureBase", tick.tree.id).value == null) return NodeStates.Failure;

            var transform = (Transform)tick.blackboard.Get("transform", tick.tree.id).value;
            var thisCreature = (CreatureBase)tick.blackboard.Get("thisCreatureBase", tick.tree.id).value;

            var targets = GameObject.FindObjectsOfType<CreatureBase>().ToList();

            var enemies = targets.FindAll(x => x.isEnemy == !isEnemy && x != thisCreature);
            if (enemies.Exists(x => Vector3.Distance(x.transform.position, transform.position) <= distance))
            {
                tick.blackboard.Set("target", enemies.Find(x => Vector3.Distance(x.transform.position, transform.position) <= distance), tick.tree.id);
                return NodeStates.Success;
            }

            return NodeStates.Failure;
        }
    }
}