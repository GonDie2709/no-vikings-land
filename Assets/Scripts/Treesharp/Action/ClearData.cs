﻿namespace InterativaSystem.Treesharp.Actions
{
    public class ClearData : Action
    {
        public string name;

        public ClearData(string name)
        {
            this.name = name;
        }
        public override NodeStates Tick(Tick tick)
        {
           tick.blackboard.Set(name, null, tick.tree.id);
           return NodeStates.Success;
        }
    }
}