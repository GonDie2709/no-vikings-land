﻿using System.Linq;
using CruzScripts.Scripts.Views.Creature;
using DG.Tweening;
using UnityEngine;

namespace InterativaSystem.Treesharp.Actions
{
    public class LookToTarget : Action
    {
        public string targetName;

        public LookToTarget(string targetName)
        {
            this.targetName = targetName;
        }

        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("transform", tick.tree.id).value == null) return NodeStates.Failure;
            if (tick.blackboard.Get(targetName, tick.tree.id).value == null) return NodeStates.Failure;

            var transform = (Transform)tick.blackboard.Get("transform", tick.tree.id).value;
            var target = (CreatureBase)tick.blackboard.Get(targetName, tick.tree.id).value;

            //transform.DOLookAt(target.transform.position, 0.6f,AxisConstraint.Z, Vector3.back).Play();
            var dir = (-transform.position + target.transform.position).normalized;
            dir = new Vector3(dir.z, dir.y, 0);
            transform.LookAt(target.transform.position, Vector3.back);
            
            return NodeStates.Success;
        }
    }
}