﻿using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace InterativaSystem.Treesharp.Actions
{
    public class MoveToTransform : Action
    {
        private string transformName;
        public MoveToTransform(string transformName)
        {
            this.transformName = transformName;
        }

        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("transform", tick.tree.id).value == null) return NodeStates.Failure;
            if (tick.blackboard.Get(transformName, tick.tree.id).value == null) return NodeStates.Failure;

            var transform = (Transform)tick.blackboard.Get("transform", tick.tree.id).value;
            var target = (CreatureBase)tick.blackboard.Get(transformName, tick.tree.id).value;
            var deltaT = (float)tick.blackboard.Get("deltaTime", tick.tree.id).value;
            var speed = (float)tick.blackboard.Get("speed", tick.tree.id).value;

            transform.position += (-transform.position + target.transform.position) * speed * deltaT;

            return NodeStates.Success;
        }
    }
}