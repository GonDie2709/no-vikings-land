﻿using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace InterativaSystem.Treesharp.Actions
{
    public class Move : Action
    {
        private string transformName;
        public Move(string transformName)
        {
            this.transformName = transformName;
        }
        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("transform", tick.tree.id).value == null) return NodeStates.Failure;
            if (tick.blackboard.Get("doMove", tick.tree.id).value == null) return NodeStates.Failure;
            if (tick.blackboard.Get(transformName, tick.tree.id).value == null) return NodeStates.Failure;

            var transform = (Transform)tick.blackboard.Get("transform", tick.tree.id).value;
            var target = (CreatureBase)tick.blackboard.Get(transformName, tick.tree.id).value;
            var doMove = (System.Action)tick.blackboard.Get("doMove", tick.tree.id).value;

            var dir = -transform.position + target.transform.position;


            tick.blackboard.Set("moveDir", new Vector2(dir.x,dir.y).normalized, tick.tree.id);
            doMove.Invoke();

            return NodeStates.Success;
        }
    }
}