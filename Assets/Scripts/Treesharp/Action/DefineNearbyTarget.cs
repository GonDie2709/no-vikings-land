﻿using System.Linq;
using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace InterativaSystem.Treesharp.Actions
{
    public class DefineNearbyTarget : Action
    {
        public bool isEnemy;
        public string targetName;

        public DefineNearbyTarget(bool isEnemy, string targetName)
        {
            this.targetName = targetName;
            this.isEnemy = isEnemy;
        }

        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("transform", tick.tree.id).value == null) return NodeStates.Failure;
            if (tick.blackboard.Get("thisCreatureBase", tick.tree.id).value == null) return NodeStates.Failure;

            var transform = (Transform) tick.blackboard.Get("transform", tick.tree.id).value;

            var targets = GameObject.FindObjectsOfType<CreatureBase>().ToList();

            var thisCreature = (CreatureBase)tick.blackboard.Get("thisCreatureBase", tick.tree.id).value;

            var enemies = targets.FindAll(x => x.isEnemy == !isEnemy && x != thisCreature);

            var min = enemies.Min(x => Vector3.Distance(x.transform.position, transform.position));
            if (enemies.Exists(x => Vector3.Distance(x.transform.position, transform.position) <= min))
            {
                tick.blackboard.Set(targetName, enemies.Find(x => Vector3.Distance(x.transform.position, transform.position) <= min),
                    tick.tree.id);
                return NodeStates.Success;
            }

            return NodeStates.Failure;
        }
    }
}