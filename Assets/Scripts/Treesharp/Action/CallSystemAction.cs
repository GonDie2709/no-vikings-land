﻿using CruzScripts.Scripts.Views.Creature;
using UnityEngine;

namespace InterativaSystem.Treesharp.Actions
{
    public class CallSystemAction: Action
    {
        private string actionName;

        public CallSystemAction(string actionName)
        {
            this.actionName = actionName;
        }
        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get(actionName, tick.tree.id).value == null) return NodeStates.Failure;

            var action = (System.Action)tick.blackboard.Get(actionName, tick.tree.id).value;

            action.Invoke();

            return NodeStates.Success;
        }
    }
}