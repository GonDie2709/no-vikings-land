﻿namespace InterativaSystem.Treesharp.Actions
{
    public class Shout : Action
    {
        public override NodeStates Tick(Tick tick)
        {
            base.Tick(tick);

            if (tick.blackboard.Get("doShout", tick.tree.id).value == null) return NodeStates.Failure;

            var shout = (System.Action)tick.blackboard.Get("doShout", tick.tree.id).value;
            shout.Invoke();

            return NodeStates.Success;
        }
    }
}