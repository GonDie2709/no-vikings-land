﻿using System.Collections.Generic;
using InterativaSystem.Treesharp.Nodes;

namespace InterativaSystem.Treesharp.Decorators
{
    public class Decorator : BaseNode
    {
        public Decorator(List<Node> children) : base(children) { }
    }
}